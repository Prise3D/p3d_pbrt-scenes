#! /bin/bash
if [ -z "$1" ]
  then
    echo "No argument supplied"
    echo "Need previous extension used"
    exit 1
fi

if [ -z "$2" ]
  then
    echo "No argument supplied"
    echo "Need new extension to use"
    exit 1
fi


main_folder="./"
prefix="p3d_"

previous=$1
new_ext=$2

for folder in $(ls -d -- ${main_folder}*/)
do
  for file in $(ls $folder)
  do
    filename=$folder$file
    filename_fixed=${filename//\/\//\/}

    # check if filename contains 
    if [[ "$file" == ${prefix}* ]]; then
        echo "Update image extension (.${previous} to .${new_ext}) into ${filename_fixed}"
        python utils/change_ext_pbrt.py --prefix ${prefix} --pbrt ${filename} --previous ${previous} --ext ${new_ext}
    fi 
  done
done