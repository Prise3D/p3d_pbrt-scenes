# main imports
import sys, os, argparse
import json


# default Sampler
# 'Integrator "path" "integer maxdepth" [ 65 ]'
INTEGRATOR_LINE = 'Integrator "path" "integer maxdepth" [ 65 ]'

def main():

    parser = argparse.ArgumentParser(description="Update integrator params of pbrt scene")

    parser.add_argument('--prefix', type=str, help='prefix of filename used', required=True)
    parser.add_argument('--pbrt', type=str, help='pbrt scene name (this one to convert)', required=True)

    args = parser.parse_args()

    p_prefix     = args.prefix
    p_pbrt       = args.pbrt
    

    output_content = ""
    # read existing pbrt file
    pbrt_file = open(p_pbrt, 'r')
    pbrt_lines = pbrt_file.readlines()

    for line in pbrt_lines:

        output_line = line
         
        # 1. Comment Transform and Camera command
        if line.startswith('Integrator'):
            output_line = INTEGRATOR_LINE + '\n'

        output_content = output_content + output_line

    # close all buffers
    pbrt_file.close()


    # update content
    pbrt_outfile = open(p_pbrt, 'w')
    pbrt_outfile.write(output_content)
    pbrt_outfile.close()

if __name__== "__main__":
    main()