# main imports
import sys, os, argparse
import json


def main():

    parser = argparse.ArgumentParser(description="Update prefix image output of pbrt scene")

    parser.add_argument('--prefix', type=str, help='prefix of filename used', required=True)
    parser.add_argument('--pbrt', type=str, help='pbrt scene name (this one to convert)', required=True)
    parser.add_argument('--previous', type=str, help='previous extension', default='png', required=True)
    parser.add_argument('--ext', type=str, help='new extension to use', default='rawls_20', required=True)

    args = parser.parse_args()

    p_prefix     = args.prefix
    p_pbrt       = args.pbrt
    p_previous   = args.previous
    p_ext        = args.ext
    

    output_content = ""
    # read existing pbrt file
    pbrt_file = open(p_pbrt, 'r')
    pbrt_lines = pbrt_file.readlines()

    for line in pbrt_lines:

        output_line = line
        
        # 1. Comment Transform and Camera command
        if line.find(p_prefix) != -1 and line.find('filename') != -1:
            output_line = line.replace('.' + p_previous, '.' + p_ext)

        output_content = output_content + output_line

    # close all buffers
    pbrt_file.close()


    # update content
    pbrt_outfile = open(p_pbrt, 'w')
    pbrt_outfile.write(output_content)
    pbrt_outfile.close()

if __name__== "__main__":
    main()