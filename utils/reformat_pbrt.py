# main imports
import sys, os, argparse
import json


# default file
PBRT_FILENAME     = 'scene.pbrt'
TUNGSTEN_FILENAME = 'scene.json'
FILM_PBRT_LINE    = 'Film "image" "integer xresolution" [ {0} ] "integer yresolution" [ {1} ] "string filename" [ "{2}" ]' 
CAMERA_PBRT_LINE  = 'Camera "perspective" "float fov" [ {0} ]'
SAMPLER_PBRT_LINE = 'Sampler "random" "integer pixelsamples" [ 64 ]'
INTE_PBRT_LINE    = 'Integrator "path" "integer maxdepth" [ 65 ]'

def main():

    parser = argparse.ArgumentParser(description="Convert .pbrt scene as expected")

    parser.add_argument('--tungsten', type=str, help='tungsten scene name', required=True)
    parser.add_argument('--pbrt', type=str, help='pbrt scene name (this one to convert)', required=True)
    parser.add_argument('--outfile', type=str, help='outfile name and path', required=True)
    parser.add_argument('--resolution', type=str, help='image resolution expected', default='1000,1000', required=True)
    parser.add_argument('--image_name', type=str, help='image output name expected', required=True)


    args = parser.parse_args()

    p_tungsten   = args.tungsten
    p_pbrt       = args.pbrt
    p_outfile    = args.outfile
    p_resolution = list(map(int, args.resolution.split(',')))
    p_image_name = args.image_name

    # Get expected information from tungsten file
    tungsten_file = open(p_tungsten, 'r')
    tungsten_data = json.load(tungsten_file)

    # read and keep information
    tungsten_scene_lookat = tungsten_data['camera']['transform']
    lookat_position = tungsten_scene_lookat['position']
    lookat_target = tungsten_scene_lookat['look_at']
    lookat_up = tungsten_scene_lookat['up']
    tungsten_scene_fov = tungsten_data['camera']['fov']

    tungsten_file.close()

    # create empty file and append necessary information about pbrt scene
    #if not os.path.exists(p_outfile):
    #    os.makedirs(p_outfile)

    pbrt_outfile = open(p_outfile, 'w')
    
    # read existing pbrt file
    pbrt_file = open(p_pbrt, 'r')
    pbrt_lines = pbrt_file.readlines()

    for line in pbrt_lines:

        # default append line
        output_line = line

        # 1. Comment Transform and Camera command
        if line.startswith('Transform') or line.startswith('Camera'):
            output_line = '#' + output_line + '\n'

        # 2. Update image information (using resolution and extension)
        if line.startswith('Film'):
            x_resolution, y_resolution = p_resolution
            output_line = FILM_PBRT_LINE.format(x_resolution, y_resolution, p_image_name) + '\n'

        # 3. Replace expected Sampler to random
        if line.startswith('Sampler'):
            output_line = SAMPLER_PBRT_LINE + '\n'

        # 4. Replace expected Integrator to path
        if line.startswith('Integrator'):
            output_line = INTE_PBRT_LINE + '\n'

        # 5. Update LookAt and Camera information using specifc fov from tungsten
        if line.startswith('WorldBegin'):
            lookat_line = 'LookAt '
            lookat_line = lookat_line + str(lookat_position[0]) + ' ' + str(lookat_position[1]) + ' ' + str(lookat_position[2]) + '\n'
            lookat_line = lookat_line + '\t\t' + str(lookat_target[0]) + ' ' + str(lookat_target[1]) + ' ' + str(lookat_target[2]) + '\n'
            lookat_line = lookat_line + '\t\t' + str(lookat_up[0]) + ' ' + str(lookat_up[1]) + ' ' + str(lookat_up[2]) + '\n'

            camera_line = CAMERA_PBRT_LINE.format(tungsten_scene_fov) + '\n'

            output_line = lookat_line + camera_line + '\n' + 'WorldBegin\n'

        pbrt_outfile.write(output_line)

    # close all buffers
    pbrt_outfile.close()
    pbrt_file.close()

if __name__== "__main__":
    main()