#! /bin/bash

tungsten_folder='tungsten-scenes'
pbrt_folder='tungsten-pbrt-scenes'

prefix='p3d_'
ext='png'

for subfolder in $(ls ${tungsten_folder})
do
    tungsten_subfolder=${tungsten_folder}/${subfolder}
    pbrt_subfolder=${pbrt_folder}/${subfolder}

    python utils/reformat_pbrt.py --tungsten ${tungsten_subfolder}/scene.json --pbrt ${pbrt_subfolder}/scene.pbrt --outfile ${pbrt_subfolder}/${prefix}${subfolder}.pbrt --resolution 1000,1000 --image_name ${prefix}${subfolder}.${ext}
done

