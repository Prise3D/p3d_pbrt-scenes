#! /bin/bash

main_folder="./"
prefix="p3d_"

for folder in $(ls -d -- ${main_folder}*/)
do
  for file in $(ls $folder)
  do
    filename=$folder$file
    filename_fixed=${filename//\/\//\/}

    # check if filename contains 
    if [[ "$file" == ${prefix}* ]]; then
        echo "Update Integrator into ${filename_fixed}"
        python utils/change_integrator_pbrt.py --prefix ${prefix} --pbrt ${filename_fixed}
    fi 
  done
done