# Overview

This repository includes a number of example scenes and
data for use with the [pbrt-v3](https://github.com/mmp/pbrt-v3) renderer,
which corresponds to the system described in the third edition of
_Physically Based Rendering_, by Matt Pharr, Wenzel Jakob, and Greg
Humphreys. (See also the [pbrt website](http://pbrt.org).)


# Data Sets

In addition to example scenes, there is some useful data for use with the
system.

* [bsdfs/](bsdfs/): this directory includes a variety of bidirectional scattering
  distribution functions (BSDFs) for use with the `FourierMaterial`. See, for
  example, the [coffee-splash](coffee-splash) scene for use of such a BSDF in a scene.
  * New versions of BSDFs for use with `FourierMaterial` can be generated
    with [layerlab](https://github.com/wjakob/layerlab/).

* [lenses/](lenses/): lens description files for a handful of real-world lens
  systems, for use with the `RealisticCamera`. See the scenes
  [villa/villa-photons.pbrt](villa/villa-photons.pbrt) and
  [sanmiguel/f6-17.pbrt](sanmiguel/f6-17.pbrt) for examples of their use.

* [spds/](spds/): measured spectral power distributions for a variety of standard
  illuminants, light sources, metals, and the squares of the Macbeth color
  checker.

* [utils/](utils/): usefull scripts for updating `.pbrt` file information quickly.

# Scenes and synthesis images

All synthesis images generated are of size `1920 x 1080` and saved into `.rawls` format for each sample estimated.

| Scene folder            | Filename                                 | Integrator | Sampler | Samples                  | Generated | Experiment step | Update reference  |
|-------------------------|------------------------------------------|------------|---------|--------------------------|-----------|-----------------|-------------------|
| `barcelona-pavilion`    | `p3d_pavilion-day-view0.pbrt`            | path       | random  | 1 to 10000               |  ✅       |   200           |   No              |
| `barcelona-pavilion`    | `p3d_pavilion-day-view1.pbrt`            | path       | random  | 20 to 10000 (by step 20) |  ✅       |   200           |   No              |
| `barcelona-pavilion`    | `p3d_pavilion-day-view2.pbrt`            | path       | random  | 20 to 10000 (by step 20) |  ✅       |   200           |   No              |
| `barcelona-pavilion`    | `p3d_pavilion-night-view0.pbrt`          | path       | random  | 1 to 10000               |  ✅       |   200           |   No              |
| `barcelona-pavilion`    | `p3d_pavilion-night-view1.pbrt`          | path       | random  | 20 to 10000 (by step 20) |  ✅       |   200           |   No              |
| `barcelona-pavilion`    | `p3d_pavilion-night-view2.pbrt`          | path       | random  | 20 to 10000 (by step 20) |  ✅       |   200           |   No              |
| `bathroom`              | `p3d_bathroom-view0.pbrt`                | path       | random  | 1 to 10000               |  ✅       |   2000          |   100000          |
| `bedroom`               | `p3d_bedroom-view0.pbrt`                 | path       | random  | 1 to 10000               |  ✅       |   2000 (not kept)  |   100000          |
| `bedroom`               | `p3d_bedroom-view1.pbrt`                 | path       | random  | 20 to 10000 (by step 20) |  ✅       |   2000 (not kept)  |   100000          |
| `bmw-m6`                | `p3d_bmw-m6-view0.pbrt`                  | path       | random  | 1 to 10000               |  ✅       |   2000 (check)  |   100000 (check)  |
| `breakfast`             | `p3d_breakfast-view0.pbrt`               | path       | random  | 20 to 10000 (by step 20) |  ❌        |   ❌             |   ❌               |
| `bunny-fur`             | `p3d_bunny-fur-view0.pbrt`               | path       | random  | 1 to 10000               |  ✅       |   200           |   No              |
| `car2`                  | `p3d_car2-view0.pbrt`                    | path       | random  | 1 to 10000               |  ✅       |   200           |   No              |
| `chopper-titan`         | `p3d_chopper-titan-view0.pbrt`           | path       | random  | 1 to 10000               |  ✅       |   2000  (check) |   100000          |
| `classroom`             | `p3d_classroom-view0.pbrt`               | path       | random  | 1 to 10000               |  ✅       |   2000          |   100000          |
| `classroom`             | `p3d_classroom-view1.pbrt`               | path       | random  | 20 to 10000 (by step 20) |  ✅       |   2000          |   100000          |
| `coffee-splash`         | `p3d_coffee-splash-view0.pbrt`           | volpath    | random  | 1 to 10000               |  ✅       |   200           |   No              |
| `contemporary-bathroom` | `p3d_contemporary-bathroom-view0.pbrt`   | path       | random  | 1 to 10000               |  ✅       |   2000          |   100000          |
| `contemporary-bathroom` | `p3d_contemporary-bathroom-view1.pbrt`   | path       | random  | 20 to 10000 (by step 20) |  ✅       |   2000          |   100000          |
| `cornel-box`            | `p3d_cornel-box-view0.pbrt`              | path       | random  | 20 to 10000 (by step 20) |  ✅       |   200           |   No              |
| `crown`                 | `p3d_crown-view0.pbrt`                   | path       | random  | 1 to 10000               |  ✅       |   200           |   No              |
| `dining-room`           | `p3d_dining-room-view0.pbrt`             | path       | random  | 1 to 10000               |  ✅       |   2000 (check)  |   100000          |
| `dining-room`           | `p3d_dining-room-view1.pbrt`             | path       | random  | 20 to 10000 (by step 20) |  ✅       |   2000 (check)  |   100000          |
| `dragon`                | `p3d_dragon-view0.pbrt`                  | path       | random  | 1 to 10000               |  ✅       |   200           |   No              |
| `echecs`                | `p3d_echecs-view0.pbrt`                  | path       | random  | 20 to 10000 (by step 20) |  ✅       |   2000 (check)  |   100000          |
| `echecs`                | `p3d_echecs-view1.pbrt`                  | path       | random  | 20 to 10000 (by step 20) |  ✅       |   2000 (check)  |   100000          |
| `echecs`                | `p3d_echecs-view2.pbrt`                  | path       | random  | 20 to 10000 (by step 20) |  ✅       |   2000 (check)  |   100000          |
| `ecosys`                | `p3d_ecosys-view0.pbrt`                  | path       | random  | 1 to 10000               |  ✅       |   200           |   No              |
| `ganesha`               | `p3d_ganesha-view0.pbrt`                 | path       | random  | 1 to 10000               |  ✅       |   200           |   No              |
| `glass`                 | `p3d_glass-view0.pbrt`                   | volpath    | random  | 1 to 10000               |  ✅       |   2000 (check)  |   100000          |
| `glass-of-water`        | `p3d_glass-of-water-view0.pbrt`          | path       | random  | 1 to 10000               |  ✅       |   200           |   No              |
| `indirect`              | `p3d_indirect-view0.pbrt`                | path       | random  | 20 to 10000 (by step 20) |  ✅       |   200           |   No              |
| `glasses`               | `p3d_glasses-view0.pbrt`                 | path       | random  | 20 to 10000 (by step 20) |  ✅       |   2000 (check)  |   100000          |
| `kitchen`               | `p3d_kitchen-view0.pbrt`                 | path       | random  | 1 to 10000               |  ✅       |   200           |   No              |
| `kitchen`               | `p3d_kitchen-view1.pbrt`                 | path       | random  | 20 to 10000 (by step 20) |  ✅       |   2000          |   100000  |
| `lamp`                  | `p3d_lamp-view.pbrt`                     | path       | random  | 1 to 10000               |  ✅       |   2000 (check)  |   100000          |
| `landscape`             | `p3d_landscape-view0.pbrt`               | path       | random  | 1 to 10000               |  ✅       |   to keep ?     |   100000          |
| `landscape`             | `p3d_landscape-view1.pbrt`               | path       | random  | 1 to 10000               |  ✅       |   to keep ?     |   100000          |
| `landscape`             | `p3d_landscape-view2.pbrt`               | path       | random  | 1 to 10000               |  ✅       |   to keep ?     |   100000          |
| `landscape`             | `p3d_landscape-view3.pbrt`               | path       | random  | 1 to 10000               |  ✅       |   200           |   No              |
| `landscape`             | `p3d_landscape-view4.pbrt`               | path       | random  | 1 to 10000               |  ✅       |   to keep ?     |   100000          |
| `living-room`           | `p3d_living-room-view0.pbrt`             | path       | random  | 1 to 10000               |  ✅       |   200           |   No              |
| `living-room`           | `p3d_living-room-view1.pbrt`             | path       | random  | 20 to 10000 (by step 20) |  ✅       |   200           |   No              |
| `living-room`           | `p3d_living-room-view2.pbrt`             | path       | random  | 20 to 10000 (by step 20) |  ✅       |   200           |   No              |
| `living-room-2`         | `p3d_living-room-2-view0.pbrt`           | path       | random  | 1 to 10000               |  ✅       |   2000 (not kept)  |   100000          |
| `living-room-2`         | `p3d_living-room-2-view1.pbrt`           | path       | random  | 20 to 10000 (by step 20) |  ✅       |   2000 (not kept)  |   100000          |
| `living-room-3`         | `p3d_living-room-3-view0.pbrt`           | volpath    | random  | 1 to 10000               |  ✅       |   2000 (check)  |   100000 (check)  |
| `living-room-3`         | `p3d_living-room-3-view1.pbrt`           | volpath    | random  | 20 to 10000 (by step 20) |  ✅       |   2000 (check)  |   100000 (check)  |
| `low_table`             | `p3d_low_table_2spheric_view0.pbrt`      | path       | random  | 20 to 10000 (by step 20) |  ✅       |   2000 (check)  |   100000 (check)  |
| `low_table`             | `p3d_low_table_coridor_view0.pbrt`       | path       | random  | 20 to 10000 (by step 20) |  ✅       |   2000 (check)  |   100000 (check)  |
| `low_table`             | `p3d_low_table_upper_view0.pbrt`         | path       | random  | 20 to 10000 (by step 20) |  ✅       |   200           |   No              |
| `low_table`             | `p3d_low_table_window-view0.pbrt`        | path       | random  | 20 to 10000 (by step 20) |  ✅       |   2000 (check)  |   100000          |
| `rubix-cube`            | `p3d_rubix-view0.pbrt`                   | path       | random  | 20 to 10000 (by step 20) |  ✅       |   2000 (check)  |   100000 (check)  |
| `sanmiguel`             | `p3d_sanmiguel-view0.pbrt`               | path       | random  | 1 to 10000               |  ✅       |   2000 (check)  |   100000 (check)  |
| `sanmiguel`             | `p3d_sanmiguel-view1.pbrt`               | path       | random  | 1 to 10000               |  ✅       |   2000 (check)  |   100000 (check)  |
| `sanmiguel`             | `p3d_sanmiguel-view2.pbrt`               | path       | random  | 1 to 10000               |  ✅       |   2000 (check)  |   100000 (check)  |
| `sanmiguel`             | `p3d_sanmiguel-view3.pbrt`               | path       | random  | 1 to 10000               |  ✅       |   2000 (check)  |   100000 (check)  |
| `spaceship`             | `p3d_spaceship-view0.pbrt`               | path       | random  | 1 to 10000               |  ✅       |   2000 (check)  |   100000          |
| `sponge`                | `p3d_sponge-5-view0.pbrt`                | path       | random  | 20 to 10000 (by step 20) |  ✅       |   200           |   No              |
| `sponge`                | `p3d_sponge-5-view1.pbrt`                | path       | random  | 20 to 10000 (by step 20) |  ✅       |   200           |   No              |
| `sponge`                | `p3d_sponge-6-view0.pbrt`                | path       | random  | 20 to 10000 (by step 20) |  ✅       |   200           |   No              |
| `sponge`                | `p3d_sponge-6-view1.pbrt`                | path       | random  | 20 to 10000 (by step 20) |  ✅       |   200           |   No              |
| `sportscar`             | `p3d_sportscar-view0.pbrt`               | path       | random  | 1 to 10000               |  ✅       |   2000 (check)  |   100000          |
| `sssdragon`             | `p3d_dragon_250-view0.pbrt`              | path       | random  | 1 to 10000               |  ✅       |   200           |   No              |
| `staircase`             | `p3d_staicase-view0.pbrt`                | path       | random  | 1 to 10000               |  ✅       |   to keep ?     |   100000          |
| `staircase`             | `p3d_staicase-view1.pbrt`                | path       | random  | 20 to 10000 (by step 20) |  ✅       |   200           |   No              |
| `staircase2`            | `p3d_staicase2-view0.pbrt`               | bdpt       | random  | 1 to 10000               |  ✅       |   200           |   No              |
| `staircase2`            | `p3d_staicase2-view1.pbrt`               | bdpt       | random  | 20 to 10000 (by step 20) |  ✅       |   2000 (check)  |   100000 (check)  |
| `structuresynth`        | `p3d_arcsphere-view0.pbrt`               | path       | random  | 1 to 10000               |  ✅       |   200           |   No              |
| `tt`                    | `p3d_tt-view0.pbrt`                      | path       | random  | 1 to 10000               |  ✅       |   200           |   No              |
| `tungsten-veach-mis`    | `p3d_tungsten-veach-mis-view0.pbrt`      | path       | random  | 1 to 10000               |  ✅       |   2000 (check)  |   100000          |
| `veach-ajar`            | `p3d_veach-ajar-view0.pbrt`              | path       | random  | 1 to 10000               |  ✅       |   2000 (check)  |   100000          |
| `veach-ajar`            | `p3d_veach-ajar-view1.pbrt`              | path       | random  | 20 to 10000 (by step 20) |  ✅       |   2000 (check)  |   100000          |
| `veach-bidir`           | `p3d_veach-bidir-view0.pbrt`             | path       | random  | 1 to 10000               |  ✅       |   2000 (check)  |   100000 (check)  |
| `villa`                 | `p3d_villa-daylight-view0.pbrt`          | path       | random  | 1 to 10000               |  ✅       |   2000 (check)  |   100000          |
| `villa`                 | `p3d_villa-daylight-view1.pbrt`          | path       | random  | 20 to 10000 (by step 20) |  ✅       |   2000 (check)  |   100000          |
| `villa`                 | `p3d_villa-daylight-view2.pbrt`          | path       | random  | 20 to 10000 (by step 20) |  ✅       |   2000 (check)  |   100000          |
| `volume-caustic`        | `p3d_caustic-view0.pbrt`                 | volpath    | random  | 1 to 10000               |  ✅       |   200           |   No              |
| `vw-van`                | `p3d_vw-van-view0.pbrt`                  | path       | random  | 1 to 10000               |  ✅       |   200           |   No              |
| `water-caustic`         | `p3d_water-caustic-view0.pbrt`           | path       | random  | 1 to 10000               |  ❌        |   ❌             |   ❌               |
| `white-room`            | `p3d_whiteroom-daytime-view0.pbrt`       | path       | random  | 1 to 10000               |  ❌        |   ❌             |   ❌               |
| `white-room`            | `p3d_whiteroom-night-view0.pbrt`         | path       | random  | 1 to 10000               |  ✅       |   2000 (check)  |   100000          |

# Usefull scripts

- `change_ext_pbrt.py`: change extension of output image into `pbrt` file.
- `change_integrator_pbrt.py`: change Integrator declaration line into `pbrt` file.
- `change_sampler_pbrt.py`: change Sampler declaration line into `pbrt` file.
- `reformat_pbrt.py`: reformat `pbrt` scene file generated from `tungsten` renderer using information from `tungsten` scene file (Camera, LookAt...).