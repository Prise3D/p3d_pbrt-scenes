# generate .ply files

```
g++ gencubesierpinski.cpp -o gencubesierpinski
```

```
./gencubesierpinski -l 6 -s 10
mv eponge_1.ply eponge_6.ply


./gencubesierpinski -l 5 -s 10
mv eponge_1.ply eponge_5.ply


./gencubesierpinski -l 4 -s 10
mv eponge_1.ply eponge_4.ply


./gencubesierpinski -l 3 -s 10
mv eponge_1.ply eponge_3.ply


./gencubesierpinski -l 2 -s 10
mv eponge_1.ply eponge_2.ply


./gencubesierpinski -l 1 -s 10

```
